// recupère la base uri de moodle
function get_ulr_ajax(){
    return window.location.href.split('mod/resource/')[0] + 'admin/tool/log/store/xapi/ajax/iframe_viewed.php';
}

// récupère le chemin de la page de l'iframe
function get_iframe_path(iframe) {
    // contiendras le chemin popre au page de l'iframe
    var path_to_page_iframe = [];
    // récupèration de l'iframe
    // récupère le lien vers la page d'accueil
    var a_home = iframe.contentWindow.document.querySelector('nav.headingSelector ul.mnu li.home div.lbl a.item');
    // si on est pas sur la page d'acceuil
    if (a_home !== null) {
        // le titre de la page d'acceuil
        //var title_home = iframe.contentWindow.document.querySelector('#header h1').textContent;
        // liens vers la page d'acceuil
//        var href_home = a_home.href;
//        // ajout de ces informations dans un tableau aux format json
//        path_to_page_iframe.push({'name': title_home, 'url': href_home});
        // url de la page courrante
        var iframe_url = iframe.contentDocument.URL;
        // tire de la page courante
        var title_iframe = iframe.contentWindow.document.getElementsByClassName('hBk_ti')[0].textContent;
        // récupère la position dans le menu de navigation
        var section_select = iframe.contentWindow.document.getElementsByClassName('sel_yes');
        // si on n'est pas sur la page d'acceuil
        if (section_select.length > 0) {
            // parent de la position dans le menu de navigation
            var section_select_parent = section_select[0].parentElement;
            // classe(s) du parent
            var section_select_parent_class = section_select_parent.className;
            // si les classes du parent sont égale sub mnu_open
            if (section_select_parent_class === 'sub mnu_open') {
                // élément html de la section supèrieure
                var up_section = section_select_parent.parentElement;
                // nom de la catègorie supèrieure
                var up_section_text = up_section.getElementsByClassName('item')[0].textContent;
                // url de la section supèrieure
                var up_section_url = up_section.getElementsByClassName('item')[0].href;
                // ajout des infos de la catègorie supière aux format json
                path_to_page_iframe.push({'name': up_section_text, 'url': up_section_url});
            }
        }
        // ajout des infos de la page courrent au format json
        path_to_page_iframe.push({'name': title_iframe, 'url': iframe_url});
    }
    return path_to_page_iframe;
}
// récupère le chemin de la page moodle
function get_moodle_path() {
    // le fil d'ariane du chemin moodle
    var ariane_wire = document.getElementsByClassName('breadcrumb')[0];
    // on separe les liens dans un tableau
    var ariane_wire_split = ariane_wire.getElementsByClassName('breadcrumb-item');
    // contiendras tous les lien au format json
    var moodle_path_to_page = [];
    var regex_url_section = /(http|https){1}:\/\/[A-Za-z0-9-_.\/]+\/course\/view\.php\?id=[0-9]+#section-[0-9]+/i;
    // pour chaque liens
    for (item = 0; item < ariane_wire_split.length; item++) {
        // s'il y à un lien
        if (ariane_wire_split[item].childElementCount > 0) {
            var url_page = ariane_wire_split[item].firstElementChild.href;
            if (url_page.substr(url_page.length - 3) !== 'my/' && !regex_url_section.test(url_page)) {
                // on le met en format json (lien et son nom) puis on l'ajoute au tableau
                moodle_path_to_page.push({'name': ariane_wire_split[item].firstElementChild.textContent, 'url': url_page});
            }
        }
    }
    return moodle_path_to_page;
}
// function date timestamps
function timestamps_date() {
    return Math.floor(Date.now() / 1000);
    ;
}
// fonction qui sert a envoié les infos de la page courante
function views_page_iframe() {
    // contioendra tous les infos de la page de l'iframe
    var json = {};
    // la date et l'heure actuelle en timestamp
    var current_timestamp = timestamps_date();
    var moodle_path_to_page = get_moodle_path();
    // get iframe
    var iframe = document.getElementById('resourceobject');
    var path_to_page_iframe = get_iframe_path(iframe);
    // création du json final
    // ajout de la date
    json['date'] = current_timestamp;
    // ajout du chemin de moodle
    json['moodle_path'] = moodle_path_to_page;
    // ajout du chemin poppre à l'iframe
    json['iframe_path'] = path_to_page_iframe;
    //console.log(json);
    if(path_to_page_iframe.length > 0){
        $.post(get_ulr_ajax(),
                {
                    'iframe_content': json
                },
                function (data) {
                    console.log(data);
                },
                'json'
                ).fail(function (jqXHR, textStatus, error) {
            console.log('bad request');
            console.log('erreur : ');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(error);
        });
   }
}
// fonction qui a envoyé les infos de test/question
function get_sub_iframe(iframe) {
    send_statement_completed = true;
    // recupère l'iframe du test
    var check_sub_iframe = iframe.contentWindow.document.getElementsByClassName('subWindow_fra');
    // si l'iframe existe bien
    if (check_sub_iframe !== null) {
        // ajout d'un evenement à l'iframe a chaque qu'il charge
        check_sub_iframe[0].addEventListener('load', function () {
            // iframe
            var sub_iframe = iframe.contentWindow.document.getElementsByClassName('subWindow_fra')[0];
            // button pour commencer le test
            var btn_start = sub_iframe.contentWindow.document.querySelector('body.subWin #root #main #content div.scroller div div p a.btnStartE');
           // button pour recommencer le test 
            var btn_restart = sub_iframe.contentWindow.document.querySelector('a.btnRestartE');
            // verifie si c'est une page de correction
            var is_solution = sub_iframe.contentWindow.document.querySelector('div.solBody');
            // verifie si une page resultat
            var is_result = sub_iframe.contentWindow.document.querySelector('div.infoScore div div span');
            // verifie si c'est la page de la validation avant la fin du test
            var is_validate_page = sub_iframe.contentWindow.document.querySelector('a.btnValidateE');
            // bouton de pour fermer l'iframe
            var is_end_page = sub_iframe.contentWindow.document.querySelector('a.btnCloseE');
            // bouton pour affcher le score quand le test a déjà finis
            var show_score_btn = sub_iframe.contentWindow.document.querySelector('a.btnScoreE');
            // verifie si c'est un test
            var is_exo = sub_iframe.contentWindow.document.querySelector('span.evalCounter');
            // recupèra le type d'activite de la question(qcm, texte a troue, etc..)
            var interactionType = null;
            // verifie si 
            var is_correction = sub_iframe.contentWindow.document.querySelector('div.score');
            // si c'est une page valide
            if (btn_start !== null || btn_restart !== null || is_solution !== null || is_result !== null || is_validate_page !== null || is_end_page !== null || show_score_btn !== null || is_exo !== null) {
                // si c'est la page d'aceuil
                if (btn_start !== null || btn_restart !== null) {
                    console.log('acceuil');
                    // heure du commencement du test
                    start_test = timestamps_date();
                    send_statement_completed = true;
                    // si il a déjà passer le test
                    if (show_score_btn !== null) {
                        // permet de ne pas renvoyer les information du test déjà fait  
                        show_score_btn.addEventListener('click', function () {
                            send_statement_completed = false;
                        });
                    }
                // si c'est une page d'exercice (qcm, texte à trou, etc..)
                } else if (is_exo !== null && is_correction === null) {
                    var number_question = sub_iframe.contentWindow.document.querySelector('span.seen').innerText;
                    console.log(number_question);
                    // heure de début de la question.
                    var start_timestamps = timestamps_date();
                    // énoncé de la question
                    var question_states = sub_iframe.contentWindow.document.querySelector('div.question div.rBk p').innerText;
                    // les cases à cocher pour des choix
                    var input_qcm = sub_iframe.contentWindow.document.querySelectorAll('ul.choiceList li.choiceList_ch div.choiceList_in input');
                    // les textinput pour les question.
                    var input_text = sub_iframe.contentWindow.document.querySelectorAll('div.exoAsw span input');
                    var input_text_hole =  sub_iframe.contentWindow.document.querySelectorAll('.txt_gap_tl span .gapInput');
                    var input_ordered_n_category = sub_iframe.contentWindow.document.querySelectorAll('.ddRepArea');
                    // bouton de la validation de la question
                    var btn_next_eval = sub_iframe.contentWindow.document.querySelector('a.nextEval');
                    // ajout d'un evenment clique à ce bouton
                    btn_next_eval.addEventListener('click', function () {
                        if(input_ordered_n_category.length > 0){
                            var ordered_dom = sub_iframe.contentWindow.document.querySelectorAll('.ddRepArea .ddDropCatch .ddDropCont .ddLabel p');
                            if(ordered_dom.length > 0){
                                interactionType = 'ordered';
                                var ordered_response = [];
                                ordered_dom.forEach(function(element_dom){
                                    ordered_response.push(element_dom.innerText);
                                });
                            }else{
                                interactionType = 'category';
                                var titles_drop_area_dom = sub_iframe.contentWindow.document.querySelectorAll('.ddRepArea .ddDropCatch .ddDropTi p');
                                var drop_area = sub_iframe.contentWindow.document.querySelectorAll('.ddRepArea .ddDropCatch .ddDropCont');
                                var category_response = response = {};
                                titles_drop_area_dom.forEach(function(title_dom){
                                    category_response[title_dom.innerText] = [];
                                });
                                drop_area.forEach(function(area, area_index){
                                    area_children = area.children;
                                    nb_child = area_children.length;
                                    if(nb_child > 0){
                                        i_child = 0;
                                        while(i_child < nb_child){
                                           category_response[titles_drop_area_dom[area_index].innerText].push(area_children[i_child].innerText);
                                           i_child++;
                                        }
                                    }else{
                                        return;
                                    }
                                });
                            }
                            console.log(ordered_response);
                            console.log(category_response);
                        // si c'est un qcm ou un qcu 
                        }else if (input_qcm.length > 0) {
                            interactionType = 'choice';
                            // contiendra les réponses choisi par l'utilisateur
                            var array_response = [];
                            // textes des choix pour la question
                            var label_qcm = sub_iframe.contentWindow.document.querySelectorAll('ul.choiceList li.choiceList_ch div.choiceList_la div.choice div.label p');
                            // pour chaque input (checkbox du qcm)
                            input_qcm.forEach(function (input, index_input) {
                                // si l'input est chocher lors du chargement de la page
                                if(input.checked){
                                    // on ajoute la le texte de cette case à cocher
                                    array_response.push(label_qcm[index_input].innerText);
                                }
                            });
                            console.log(array_response);
                        // si c'est une question avec un texte/nombre à entrer au clavier
                        } else if (input_text.length > 0) {
                            // definie le type de question
                            interactionType = 'fill-in';
                            // partie pour les text input
                            // contiendra la response
                            var text_response = input_text[0].value;
                            console.log(text_response);
                        // si c'est un texte à trou
                        } else if (input_text_hole.length > 0) {
                            // défintion du type de l'activity 
                            interactionType = 'sequencing';
                            // création d'un tableau de même longueur que le nombre de trou dans le texte
                            var array_hole_fill = Array.from('_'.repeat(input_text_hole.length));
                            input_text_hole.forEach(function (input_hole, index_input_hole){
                                array_hole_fill[index_input_hole] = input_hole.value;
                            });
                            console.log(array_hole_fill);
                        // partie pour l'ajout de nouveaux format de question
                        }else {
                            interactionType = 'none';
                            console.log('a faire');
                        }
                        console.log(interactionType);
                        // c'est un des formats de question gérer
                        if (interactionType !== 'none') {
                            // on envoie les donnée de la question(enoncé, réponse, etc..) en ajax
                            var json_eval = {
                                'question_states': question_states,
                                'text': (typeof text_response !== 'undefined' ? text_response : null),
                                'qcm': (typeof array_response !== 'undefined' ? array_response : null),
                                'hole_text': (typeof array_hole_fill !== 'undefined' ? array_hole_fill : null),
                                'ordered': (typeof ordered_response !== 'undefined' ? ordered_response : null),
                                'category': (typeof category_response !== 'undefined' ? category_response : null),
                                'activity_type': interactionType,
                                'moodle_path': get_moodle_path(),
                                'iframe_path': get_iframe_path(iframe),
                                'number_question': number_question,
                                'date': timestamps_date(),
                                'duration': timestamps_date() - start_timestamps
                            };
                            $.post(get_ulr_ajax(),
                                    {
                                        'response_question': json_eval
                                    },
                                    function (data) {
                                        console.log(data);
                                    },
                                    'json'
                                    ).fail(function (jqXHR, textStatus, error) {
                                console.log('bad request');
                                console.log('erreur : ');
                                console.log(jqXHR);
                                console.log(textStatus);
                                console.log(error);
                            });
                        }
                    });
                // si c'est la page des resultats
                } else if (is_result !== null) {
                    console.log('resultat');
                    console.log(send_statement_completed);
                    // si l'utilisateur n'est pas sur cette page via le bouton voir le résultat de la page d'acceuil
                    if (send_statement_completed) {
                        // phrase qui contient le score
                        var score_sentence = is_result.innerText;
                        // regex pour récupère le score
                        var regex_extract_score = /[0-9]+%/;
                        // contiendra le score
                        var score_test;
                        // si la phrase est égale a "Vous avez bien répondu à toutes les questions !" on met le score à 100
                        if (score_sentence === 'Vous avez bien répondu à toutes les questions !') {
                            score_test = 100;
                        // sinon on extrait le score et on le convertit en int(peut-être en float serait mieux)
                        } else {
                            score_test_str = score_sentence.match(regex_extract_score)[0];
                            score_test = parseInt((score_test_str.substring(0, score_test_str.length - 1)));
                        }
                        // on récupère les infos du test
                        var json_completed_test = {
                            'score': score_test,
                            'moodle_path': get_moodle_path(),
                            'iframe_path': get_iframe_path(iframe),
                            'date': timestamps_date(),
                            'duration': timestamps_date() - start_test
                        };
                        console.log(json_completed_test);
                        // on les envoie ces infos en ajax
                        $.post(get_ulr_ajax(),
                                {
                                    'test_completed': json_completed_test
                                },
                                function (data) {
                                    console.log(data);
                                },
                                'json'
                                ).fail(function (jqXHR, textStatus, error) {
                            console.log('bad request');
                            console.log('erreur : ');
                            console.log(jqXHR);
                            console.log(textStatus);
                            console.log(error);
                        });
                    }
                }
            }
        });
    }
}

window.onload = function () {
    var event_is_already = false;
    // recupèration de l'url
    var url = window.location.href;
    // format de l'url voulu 
    var regex_url = /http(s){0,1}:\/\/[A-Za-z0-9-_.\/]+\/mod\/resource\/view\.php\?id=[0-9]+/i;
    // si l'url est dans le bon format (activité de type ressource)
    if (regex_url.test(url)) {
        // récupère les iframes de la page
        var iframe = $('iframe');
        // verifie si il y a bien un iframe
        if (iframe.length > 0) {
            // lance le traîtement xapi au chargement de la page
            //views_page_iframe();
            // et à chaque chagment de page dans l'iframe on relance le traîtement xapi
            iframe.on('load', function () {
                views_page_iframe();
                iframe_js_natif = document.getElementById('resourceobject');
                var btn_enter_test = iframe_js_natif.contentWindow.document.querySelector('#document div.scroller section.hBk div.hBk_co div.gotoEval a.btnEval');
                if (btn_enter_test !== null) {
                    //console.log(btn_enter_test);
                    btn_enter_test.addEventListener('click', function () {
                        if(!event_is_already){
                            get_sub_iframe(iframe_js_natif);
                            event_is_already = true;
                        }
                    });
                }
            });
        }
    }
};