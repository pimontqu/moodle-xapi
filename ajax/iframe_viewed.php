<?php
require_once('../vendor/rusticisoftware/tincan/autoload.php');
require_once('../../../../../../config.php');
require_login();

use TinCan\RemoteLRS;
use TinCan\Statement;
use TinCan\Agent;
use TinCan\AgentAccount;
use TinCan\Verb;
use TinCan\Activity;
use TinCan\ActivityDefinition;
use TinCan\LanguageMap;
use TinCan\ContextActivities;
use TinCan\Context;
use TinCan\Result;
use TinCan\Score;
// récupère le endpoint du lrs dans la config
$end_point = get_config('logstore_xapi', 'endpoint');
// récupère les identifiants pour la connexion au lrs
$user_lrs = get_config('logstore_xapi', 'username');
$pwd_lrs = get_config('logstore_xapi', 'password');
$send_mbox = get_config('logstore_xapi', 'mbox');
$send_username = get_config('logstore_xapi', 'send_username');
// base uri du moodle
$base_uri = $CFG->httpswwwroot;
// connexion au lrs
$remoteLrs = new RemoteLRS($end_point, '1.0.1', $user_lrs, $pwd_lrs);
// renseigne l'utilisateur pour le statement
$actor = set_actor($USER, $base_uri, $send_mbox, $send_username);
if (isset($_POST['iframe_content'])) {
    // recupère les données envoyé danjs la requètes
    $data_post = $_POST['iframe_content'];
    // date courante
    $timestamp = $data_post['date'];
    // liens d'accès a la page moodle et celle a l'interieur de moodle
    $iframe_path = $data_post['iframe_path'];
    $moodle_path = $data_post['moodle_path'];
    // ajout de la page d'acceuil moodle au debut de l'array des liens moodle
    array_unshift($moodle_path, array('name' => $COURSE->fullname, 'url' => $base_uri));
    // récupère le nom et l'url de la page courante de moodle
    $iframe_current_url = $iframe_path[count($iframe_path) - 1]['url'];
    $iframe_current_name = $iframe_path[count($iframe_path) - 1]['name'];
    // renseigne le timestamp pour le statement 
    $timestamp_iso = set_timestamp($timestamp);
    // renseigne l'activité pour le statement
    $activity = set_activity($iframe_current_url, $iframe_current_name, 'http://id.tincanapi.com/activitytype/resource');
    // renseigne le context pour le statement
    $context = set_context($moodle_path, $iframe_path);
    // créer le statement
    $statement = viewed_statement($actor, $activity, $timestamp_iso, $context);
    // sauvegarde le statement dans le lrs
    $response = $remoteLrs->saveStatement($statement);
    // message pour le debug
    if ($response->success) {
        $feedback = 'success';
    } else {
        $feedback = $response->content;
    }
    // retourne les reponses le pour le debug
    echo json_encode(array('iso' => $timestamp_iso, 'feedback' => $feedback, 'path_moodle' => $moodle_path, 'session' => $_SESSION, 'user' => $USER));
}
if (isset($_POST['response_question'])) {
    $answer_post = $_POST['response_question'];
    $activity_type = $answer_post['activity_type'];
    if ($activity_type == 'choice' || $activity_type == 'sequencing' || $activity_type == 'ordered') {
        $response = set_array_response('qcm', $answer_post);
    }
    if ($activity_type == 'fill-in') {
        $response = $answer_post['text'];
    }
    if($activity_type == 'category'){
        if(key_exists('category', $answer_post)){
            $response_array = $answer_post['category'];
            $response_array_key = array_keys($response_array);
            $response = '';
            foreach ($response_array as $key => $response_value){
                $response .= $key . '[:]';
                $response .= implode('[,]', $response_value);
                if(array_search($key, $response_array_key) < count($response_array_key) - 1){
                    $response .= '[/]';
                }
            }
        }else{
            $response = '';
        }
    }
    if ($activity_type != 'none') {
        $question_states = $answer_post['question_states'];
        // renseigne le timestamp pour le statement 
        $timestamp_iso = set_timestamp($answer_post['date']);
        $iframe_path = $answer_post['iframe_path'];
        // récupère le nom et l'url de la page courante de moodle
        $iframe_current_url = $iframe_path[count($iframe_path) - 1]['url'];
        $iframe_current_url .= '?question=' . $answer_post['number_question'];
        array_push($iframe_path, array('url' => $iframe_current_url, 'name' => $question_states));
        $moodle_path = $answer_post['moodle_path'];
        array_unshift($moodle_path, array('name' => $COURSE->fullname, 'url' => $base_uri));
        // renseigne l'activité pour le statement
        $activity = set_activity($iframe_current_url, $question_states, 'http://adlnet.gov/expapi/activities/cmi.interaction', $activity_type);
        // renseigne le context pour le statement
        $context = set_context($moodle_path, $iframe_path);
        // paremètre le champs results
        $result = set_result_answer($response);
        // créer le statement
        $statement = ansewered_completed_statement($actor, $activity, $timestamp_iso, $context, $result,'http://adlnet.gov/expapi/verbs/answered', 'answered');
        // sauvegarde le statement dans le lrs
        $response_lrs = $remoteLrs->saveStatement($statement);
        // message pour le debug
        if ($response_lrs->success) {
            $feedback = 'success';
        } else {
            $feedback = $response_lrs->content;
        }
        echo json_encode(array('response' => $response, 'status' => $feedback));
    }
}
if (isset($_POST['test_completed'])) {
    $completed_post = $_POST['test_completed'];
    // renseigne le timestamp pour le statement 
    $timestamp_iso = set_timestamp($completed_post['date']);
    $duration = $completed_post['duration'];
    // score obtenue au test
    $score_test = (float)$completed_post['score'];
    //$score_test = int($score_test);
    $iframe_path = $completed_post['iframe_path'];
    // récupère le nom et l'url de la page courante de moodle
    $iframe_current_url = $iframe_path[count($iframe_path) - 1]['url'];
    $iframe_current_name = $iframe_path[count($iframe_path) - 1]['name'];
    $moodle_path = $completed_post['moodle_path'];
    array_unshift($moodle_path, array('name' => $COURSE->fullname, 'url' => $base_uri));
    // renseigne l'activité pour le statement
    $activity = set_activity($iframe_current_url, $iframe_current_name, 'http://adlnet.gov/expapi/activities/cmi.interaction');
    // renseigne le context pour le statement
    $context = set_context($moodle_path, $iframe_path);
    
    // paremètre le champs results
    $result = set_result_completed($score_test, $duration);
    // créer le statement
    $statement = ansewered_completed_statement($actor, $activity, $timestamp_iso, $context, $result, 'http://adlnet.gov/expapi/verbs/completed', 'completed');
    // sauvegarde le statement dans le lrs
    $response_lrs = $remoteLrs->saveStatement($statement);
    // message pour le debug
    if ($response_lrs->success) {
        $feedback = 'success';
    } else {
        $feedback = $response_lrs->content;
    }
    echo json_encode(array('score' => $score_test, 'staus' => $feedback));
}

// créez le statement viewed pour le iframe
function viewed_statement($actor, $activity, $timestamp, $context) {
    // crée le verb
    $verb = set_verb('http://id.tincanapi.com/verb/viewed', 'viewed');
    # crée le statement en renseignant l'actor, le verb et l'activité(object)
    $statement = new Statement(array(
        'actor' => $actor,
        'verb' => $verb,
        'object' => $activity
    ));
    // renseigne le timestamp
    $statement->setTimestamp($timestamp);
    // renseigne le context
    $statement->setContext($context);
    // retourne le statement
    return $statement;
}

// crée un statement answered
function ansewered_completed_statement($actor, $activity, $timestamp, $context, $result, $verb_id, $verb_name) {
    // crée le verb
    $verb = set_verb($verb_id, $verb_name);
    $statement = new Statement(array(
        'actor' => $actor,
        'verb' => $verb,
        'object' => $activity
    ));
    // renseigne le timestamp
    $statement->setTimestamp($timestamp);
    // renseigne le context
    $statement->setContext($context);
    $statement->setResult($result);
    // retourne le statement
    return $statement;
}

// convertit le timestamps dans le bon format
function set_timestamp($timestamp) {
    return date('c', $timestamp);
}

// crée l'actor 
function set_actor($userObject, $server, $mbox, $user_name) {
    $user_fullname = $userObject->firstname . ' ' . $userObject->lastname;
    // crée un objent Agent(actor)
    $actor = new Agent();    
    $actor->setName($user_fullname);
    // crée un objet AgentAccount(extension objet actor)
    $account = new AgentAccount();
    if($mbox){
        $actor->setMbox('mailto:' . $userObject->email);
        $account->setName($userObject->email);
    } elseif ($user_name) {
        $account->setName($userObject->username);
    }else{
        $account->setName($userObject->id);
    }
    //$account->setName('test');
    // renseigne le page de l'account(page d'accueil du moodle)
    $account->setHomePage($server);
    // renseigne l'account a l'actor
    $actor->setAccount($account);
    // retourne l'actor
    return $actor;
}

// crée le verb
function set_verb($id, $display) {
    // crée un objet verb
    $verb = new Verb();
    // renseigne l'id du verb
    $verb->setId($id);
    // renseigne le nom du verb
    $verb->setDisplay(new LanguageMap(array('en' => $display)));
    //retourne le verbe
    return $verb;
}

// crée l'activity (object)
function set_activity($url, $name, $type, $interactionType = null) {
    // crée l'objet activity (url)
    $activity = new Activity(array('id' => $url));
    // crée l'activity definition(extension de l'objet activity)
    $activityDefinition = new ActivityDefinition();
    // nom de l'activity
    $activityDefinition->setName(new LanguageMap(array('en' => $name)));
    // renseigne le type de l'activity
    $activityDefinition->setType($type);
    if($interactionType != null){
        $interactionTypeValid = ['true-false', 'choice', 'fill-in', 'long-fill-in', 'matching', 'performance', 'sequencing', 'likert', 'numeric', 'other'];
        if(!in_array($interactionType, $interactionTypeValid)){
            $interactionType = 'other';
        }
        $activityDefinition->setInteractionType($interactionType);
    }
    // renseigne le definition à l'activity
    $activity->setDefinition($activityDefinition);
    return $activity;
}

// crée le context activity(chemin de liens vers l'activity)
function set_context_activity($moodle_path, $iframe_path) {
    // regroupera tous les activity pour aller à l'activité
    $grouping = [];
    // crée l'objet ContextActivities
    $contextActivity = new ContextActivities();
    // pour chaque liens moodle
    foreach ($moodle_path as $activity_moodle) {
        // push dans l'array grouping l'activité
        array_push($grouping, set_activity_type($activity_moodle));
    }
    // si il y a plus d'un liens
    if (count($iframe_path) > 1) {
        // supprime le dernier liens de l'iframe(activité courante)
        array_pop($iframe_path);
        // pour chaque liens
        foreach ($iframe_path as $activity_iframe) {
            // push dans l'array grouping l'activité
            array_push($grouping, set_activity_type($activity_iframe));
        }
    }
    $contextActivity->setGrouping($grouping);
    return $contextActivity;
}

// crée une activity avec le bon type
function set_activity_type($activity) {
    // regex pour savoir de quel type d'activity il s'agit
    $regex_course = '/\/course\/view\.php\?id=[0-9]+/i';
    $regex_ressoure = '/\/mod\/resource\/view\.php\?id=[0-9]+/i';
    // crée l'activité avec le bon type
    if (preg_match($regex_course, $activity['url'])) {
        $current_activity = set_activity($activity['url'], $activity['name'], 'http://id.tincanapi.com/activitytype/lms/course');
    } elseif (preg_match($regex_ressoure, $activity['url'])) {
        $current_activity = set_activity($activity['url'], $activity['name'], 'http://id.tincanapi.com/activitytype/resource');
    } else {
        $current_activity = set_activity($activity['url'], $activity['name'], 'http://id.tincanapi.com/activitytype/lms');
    }
    // retourne l'activity
    return $current_activity;
}

// crée le context
function set_context($moodle_path, $iframe_path) {
    // crée l'objet context
    $context = new Context();
    // renseigne la platform
    $context->setPlatform('moodle');
    // renseigne la lang
    $context->setLanguage('en');
    // renseigne le contextActivities
    $context->setContextActivities(set_context_activity($moodle_path, $iframe_path));
    // retourne le context
    return $context;
}

function set_result_answer($response) {
    $result = new Result();
    $result->setCompletion(true);
    $result->setResponse($response);
    //$result->setDuration($duration);
    return $result;
}

function set_result_completed($score_test, $duration){
    $result = new Result();
    $score = new Score();
    $result->setCompletion(true);
    $result->setDuration('PT' . $duration . 'S');
    $score->setMax(100);
    $score->setMin(0);
    $score->setRaw($score_test);
    $score->setScaled($score_test / 100);
    $result->setScore($score);
    return $result;
}

function set_array_response($type, $ajax_request){
    if (array_key_exists($type, $ajax_request)) {
        $response_array = $ajax_request[$type];
        if(count($response_array) == 0){
            $response = '';
        }elseif (count($response_array) == 1) {
            $response = $response_array[0];
        } else {
            $response = implode('[,]', $response_array);
        }
    } else {
        $response = '';
    }
    return $response;
}