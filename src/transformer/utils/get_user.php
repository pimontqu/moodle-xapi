<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace src\transformer\utils;

defined('MOODLE_INTERNAL') || die();

function get_user(array $config, \stdClass $user) {
    //file_put_contents('conf.txt', print_r($config, TRUE));
    $fullname = get_full_name($user);
    // The following email validation matches that in Learning Locker
    $hasvalidemail = mb_ereg_match("[A-Z0-9\\.\\`\\'_%+-]+@[A-Z0-9.-]+\\.[A-Z]{1,63}$", $user->email, "i");

    if (array_key_exists('send_mbox', $config) && $config['send_mbox'] == true && $hasvalidemail) {
        return [
            'name' => $fullname,
            'mbox' => 'mailto:' . $user->email,
        ];
    }

    if (array_key_exists('send_username', $config) && $config['send_username'] === true) {
        return [
            'name' => $fullname,
            'account' => [
                'homePage' => $config['app_url'],
                'name' => $user->username,
            ],
        ];
    }

    return [
        'name' => $fullname,
        'account' => [
            'homePage' => $config['app_url'],
            'name' => strval($user->id),
        ],
    ];
}

function v5($namespace, $name) {
    if (!is_valid($namespace)){
        return false;
    }
// Get hexadecimal components of namespace
    $nhex = str_replace(array('-', '{', '}'), '', $namespace);

// Binary Value
    $nstr = '';

// Convert Namespace UUID to bits
    for ($i = 0; $i < strlen($nhex); $i += 2) {
        $nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
    }

// Calculate hash value
    $hash = sha1($nstr . $name);

    return sprintf('%08s-%04s-%04x-%04x-%12s',
            // 32 bits for "time_low"
            substr($hash, 0, 8),
            // 16 bits for "time_mid"
            substr($hash, 8, 4),
            // 16 bits for "time_hi_and_version",
// four most significant bits holds version number 5
            (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
// 8 bits for "clk_seq_low",
// two most significant bits holds zero and one for variant DCE1.1
            (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
            // 48 bits for "node"
            substr($hash, 20, 12)
    );
}

function is_valid($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?' .
                    '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}
